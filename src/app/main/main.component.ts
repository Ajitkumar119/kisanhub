import { Component, OnInit } from '@angular/core';
import {WeatherreportService} from '../weatherreport.service';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

export interface Food {
  value : string;
  viewValue : string;
}
export interface City {
  // value : string;
  viewValue : string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers:[WeatherreportService]
})
export class MainComponent implements OnInit {
date=undefined;
dt=undefined;
metric=undefined;
city=undefined;
fromdate=undefined;
todate=undefined;
valuedata=[];
  yeardata=[];
  monthdata=[];
  value={};
  // year={};
  month={};
select={};
newArr=[];
newArr1=[];
visibility=false;
visibility1=false;
fromdtcheck=undefined;
todtcheck=undefined;
userselect={};
barChartData:any[] =[{data:[],label:'Series A'}];
  foods: Food[] = [
    {value : 'max-temp', viewValue: 'Tmax'},
    {value : 'min-temp', viewValue: 'Tmin'},
    {value : 'rainfall', viewValue: 'Rainfall'}
  ];
  cities: City[] = [
    {viewValue: 'UK'},
    {viewValue: 'England'},
    {viewValue: 'Scotland'},
    {viewValue: 'Wales'}

  ];
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[]=[];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  // public barChartData:any[] =[{data:[],label:'Series A'}];
  
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 
  // public randomize():void {
  //   // Only Change 3 values
  //   let data = [
  //     Math.round(Math.random() * 100),
  //     59,
  //     80,
  //     (Math.random() * 100),
  //     56,
  //     (Math.random() * 100),
  //     40];
  //   let clone = JSON.parse(JSON.stringify(this.barChartData));
  //   clone[0].data = data;
  //   this.barChartData = clone;
  //   /**
  //    * (My guess), for Angular to recognize the change in the dataset
  //    * it has to change the dataset variable directly,
  //    * so one way around it, is to clone the data, change it and then
  //    * assign it;
  //    */
  // }
  public lineChartData:Array<any> =[{data:[],label:'Series A'},{data:[],label:'Series B'}];
  
  public lineChartLabels:Array<any>=[];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }


  constructor(private weather:WeatherreportService) { }

  ngOnInit() {
    this.date=new Date()
    this.dt=this.date.toLocaleDateString()
    console.log(this.dt)
    
  }
  metricsselect(e){
    console.log(e.target.innerHTML);
    var metric=e.target.innerHTML
    this.metric=metric.trim()
    this.userselect['metric']=this.metric;
  }
  cityselect(e){
    console.log(e.target.innerHTML);
    var city=e.target.innerHTML
    this.city=city.trim()
    this.userselect['city']=this.city;
  }
  fromdt(e){
    console.log(e.target.value);
    this.fromdate=e.target.value
    this.userselect['fromdt']=this.fromdate;
  }
  todt(e){
    console.log(e.target.value);
    this.todate=e.target.value
    this.userselect['todate']=this.todate;
  }
  onsubmit(){
    if(this.userselect['fromdt']==undefined){
      this.userselect['fromdt']=this.dt;
    }
    if(this.userselect['todate']==undefined){
      this.userselect['todate']=this.dt;
    }
    this.fromdtcheck=new Date( this.userselect['fromdt']).getTime()/1000;
    this.todtcheck=new Date(this.userselect['todate']).getTime()/1000;
    if(this.fromdtcheck < this.todtcheck){
      this.weather.getreport(this.userselect)
      .subscribe((res) => {
        console.log(res)
        var fromyr=new Date(this.userselect['fromdt']).getFullYear();
        var toyr= new Date(this.userselect['todate']).getFullYear();
          if(fromyr==toyr){
            this.visibility=true;
            var frommth=new Date(this.userselect['fromdt']).getMonth()+1;
            var tomth=new Date(this.userselect['todate']).getMonth()+1;
            this.newArr=[];
            this.barChartData=[{data:[],label:'Series A'}];
            this.barChartLabels=[];
            for(var i=0;i < res['length'];i++){
              if(i<res['length']){
                if(res[i].year == fromyr){
                  this.barChartLabels.push(res[i].month);
                  if(res[i].month >= frommth && res[i].month <=tomth){
                    this.newArr.push(res[i].value);
                  }
                  else{
                    this.newArr.push(0);
                  }
                  // this.newArr.push(res[i].value);
                  console.log(this.newArr);
                  this.barChartData=[{data:this.newArr,label:'Series A'}]
                }
              }else{
                break
              }
              

            }
          }
          else{
            this.visibility1=true;
            var frommth=new Date(this.userselect['fromdt']).getMonth()+1;
            var tomth=new Date(this.userselect['todate']).getMonth()+1;
            var fromyr=new Date(this.userselect['fromdt']).getFullYear();
            var toyr=new Date(this.userselect['todate']).getFullYear();
            this.barChartData=[{data:[], label:'Series A'}]
            this.newArr1=[]
            this.barChartLabels=[]
            for(var j = 0;j< res['length'];j++){
              var userfrmdata=fromyr+0.01*frommth;
              var usertodata=toyr+0.01*tomth;
              var resdata=res[j].year+0.01*res[j].month;
              if(resdata<=usertodata){
                if(resdata >= userfrmdata){
                  this.newArr1.push(res[j].value)
                  var month = res[j].month.toString();
                  var coma =",";
                  var year = res[j].year.toString();
                  var str = month.concat(coma,year);
                  this.barChartLabels.push(str)
                  console.log(this.barChartLabels)  
                }
              }
              
              else{
                break;
              }
            }
            this.barChartData=[{data:this.newArr1, label:'Series A'}]
        }
      })
    }
    else{
      alert("Choose correct date");
    }
    
    }
}