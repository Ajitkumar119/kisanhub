import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherreportService {
  string_url:string;
  constructor(private http:HttpClient) { 
    this.string_url='https://s3.eu-west-2.amazonaws.com/interview-question-data/metoffice/'
  }
  getreport(data){
    return this.http.get(this.string_url+data.metric+'-'+data.city+'.json',{})
  }
}
